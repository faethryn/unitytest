﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SprintingState : BaseState
{
    [SerializeField]
    private CharacterStateMachine _CSM;
    public SprintingState(CharacterStateMachine stateMachine) : base("Moving", stateMachine)
    {
        _CSM = stateMachine;
    }

    [SerializeField] private float speed = 30f;

    public CharacterController playerController;
    public Transform cam;

    [SerializeField] private float turnSmoothTime = 0.1f;
    float turnSmoothVelocity;

    [SerializeField]
    [Range(0.5f, 100f)]
    private float _jumpHeight;
    private Vector3 _jumpForce;
    private bool _jump;

    public Vector3 _velocity;
    [SerializeField] private Vector3 _maxVelocity;
    [SerializeField] private bool _speedlimit;

    [SerializeField] private float _dragOnGround;
    [SerializeField] private float _minimumSpeed;

    public override void Enter()
    {
        base.Enter();
        _jumpForce = -Physics.gravity.normalized * Mathf.Sqrt(2 * Physics.gravity.magnitude * _jumpHeight);

    }
    public override void UpdateLogic()
    {
        base.UpdateLogic();
        StateChecks();

    }

   

    public override void UpdatePhysics()
    {
        base.UpdatePhysics();
       
        ApplyGravity();
        ApplyGround();
        ApplyGroundDrag();
        ApplyJump();
        LimitSpeed();
        GroundMovement();
        
        MovementStabilize();
        
    }

    private void StateChecks()
    {
        SprintCheck();
        FlyingCheck();
    }

    private void FlyingCheck()
    {
        if (!playerController.isGrounded && Input.GetButtonDown("Jump"))
        {
            _CSM.ChangeState(_CSM.FlyState);
        }
    }

    private void SprintCheck()
    {
       if(!Input.GetButton("Sprint"))
        {
            _CSM.MovingState._velocity = _velocity;
            _CSM.ChangeState(_CSM.MovingState);
        }
    }

    private void MovementStabilize()
    {
        if (Mathf.Abs(_velocity.x) > _minimumSpeed && Input.GetAxis("Horizontal") == 0 && Input.GetAxis("Vertical") == 0)
        {
            _velocity.x = 0;

        }
        if (Mathf.Abs(_velocity.z) > _minimumSpeed && Input.GetAxis("Horizontal") == 0 && Input.GetAxis("Vertical") == 0)
        {
            _velocity.z = 0;
        }





    }

    private void LimitSpeed()
    {
        if (_speedlimit)
        {
            _velocity = new Vector3(Mathf.Clamp(_velocity.x, -_maxVelocity.x, _maxVelocity.x), _velocity.y, Mathf.Clamp(_velocity.z, -_maxVelocity.z, _maxVelocity.z));
        }
    }

    private void ApplyGroundDrag()
    {
        if (playerController.isGrounded)
        {
            _velocity = _velocity * (1 - Time.fixedDeltaTime * _dragOnGround);

        }
    }

    private void GroundMovement()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");
        Vector3 direction = new Vector3(horizontal, 0f, vertical).normalized;
        if (direction.magnitude >= 0.1f)
        {
            float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(_CSM.transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
            _CSM.transform.rotation = Quaternion.Euler(0f, angle, 0f);

            //Rotation(direction);
            Vector3 moveDirection = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
            //playerController.Move(moveDirection.normalized * speed * Time.deltaTime);
            _velocity += moveDirection.normalized * speed * Time.fixedDeltaTime;



        }
        playerController.Move(_velocity * Time.fixedDeltaTime);
    }

    //private void Rotation(Vector3 InputDirection)
    //{
    //    float targetAngle = Mathf.Atan2(InputDirection.x, InputDirection.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
    //    float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
    //    transform.rotation = Quaternion.Euler(0f, angle, 0f);

    //}

    private void ApplyGround()
    {
        if (playerController.isGrounded)
        {
            _velocity -= Vector3.Project(_velocity, Physics.gravity.normalized);
        }
    }

    private void ApplyJump()
    {
        if (Input.GetButton("Jump") && playerController.isGrounded)
        {
            _velocity += _jumpForce;

        }

    }

    private void ApplyGravity()
    {
        if (!playerController.isGrounded)
        {
            _velocity += Physics.gravity * Time.fixedDeltaTime;

        }

    }
    public override void Exit()
    {
        _velocity = new Vector3(0, 0, 0);
        base.Exit();
    }

}
