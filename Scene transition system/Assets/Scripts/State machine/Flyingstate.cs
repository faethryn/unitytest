﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Flyingstate : BaseState
{
    
    [SerializeField]
    private CharacterStateMachine _CSM;
    public Flyingstate(CharacterStateMachine stateMachine) : base("Moving", stateMachine)
    {
        _CSM = stateMachine;
    }
    [SerializeField] private float speed = 30f;

    public CharacterController playerController;
    public Transform cam;

    [SerializeField] private float turnSmoothTime = 0.1f;
    float turnSmoothVelocity;

    private bool _appliedGravity = false;

    public Vector3 _velocity;
    [SerializeField] private Vector3 _maxVelocity;
    [SerializeField] private bool _speedlimit;

    [SerializeField] private float _dragOnGround;
    [SerializeField] private float _minimumSpeed;

    [SerializeField] private float _downwardMovement;
    public override void Enter()
    {
        base.Enter();
       

    }
    public override void UpdateLogic()
    {
        base.UpdateLogic();
        GroundState();
    }

    public override void UpdatePhysics()
    {
        base.UpdatePhysics();
        ApplyGravity();
        
        ApplyAirDrag();
       
        LimitSpeed();
        AirMovement();
       
        MovementStabilize();

    }

   
    private void MovementStabilize()
    {
        if (Mathf.Abs(_velocity.x) > _minimumSpeed && Input.GetAxis("Horizontal") == 0 && Input.GetAxis("Vertical") == 0)
        {
            _velocity.x = 0;

        }
        if (Mathf.Abs(_velocity.z) > _minimumSpeed && Input.GetAxis("Horizontal") == 0 && Input.GetAxis("Vertical") == 0)
        {
            _velocity.z = 0;
        }





    }

    private void LimitSpeed()
    {
        if (_speedlimit)
        {
            _velocity = new Vector3(Mathf.Clamp(_velocity.x, -_maxVelocity.x, _maxVelocity.x), _velocity.y, Mathf.Clamp(_velocity.z, -_maxVelocity.z, _maxVelocity.z));
        }
    }

    private void ApplyAirDrag()
    {
        float ystorage = _velocity.y;
        
            _velocity = _velocity * (1 - Time.deltaTime * _dragOnGround);
        _velocity.y = ystorage;
      
    }

    private void AirMovement()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");
        Vector3 direction = new Vector3(horizontal, 0f, vertical).normalized;
        if (direction.magnitude >= 0.1f)
        {
            float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(_CSM.transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
            _CSM.transform.rotation = Quaternion.Euler(0f, angle, 0f);

            //Rotation(direction);
            Vector3 moveDirection = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
            //playerController.Move(moveDirection.normalized * speed * Time.deltaTime);
            _velocity += moveDirection.normalized * speed * Time.deltaTime;



        }
        playerController.Move(_velocity * Time.deltaTime);
    }

    //private void Rotation(Vector3 InputDirection)
    //{
    //    float targetAngle = Mathf.Atan2(InputDirection.x, InputDirection.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
    //    float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
    //    transform.rotation = Quaternion.Euler(0f, angle, 0f);

    //}

    private void GroundState()
    {
        if (playerController.isGrounded || Input.GetButtonDown("Jump"))
        {
            _CSM.ChangeState(_CSM.MovingState);
        }
        

    }

   

    private void ApplyGravity()
    {
        if (!playerController.isGrounded && _appliedGravity == false)
        {


            _velocity.y += _downwardMovement * Physics.gravity.y * Time.fixedDeltaTime;
            if(_velocity.y <= (_downwardMovement * Physics.gravity.y * Time.fixedDeltaTime) && _appliedGravity == false)
            {
                _appliedGravity = true;
                _velocity.y = _downwardMovement * Physics.gravity.y * Time.fixedDeltaTime;

            }
               
           

        }

    }
    public override void Exit()
    {
        _velocity = new Vector3(0, 0, 0);
        _appliedGravity = false;
        base.Exit();
    }
}
